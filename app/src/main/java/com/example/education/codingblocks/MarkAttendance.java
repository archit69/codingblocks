package com.example.education.codingblocks;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class MarkAttendance extends AppCompatActivity {

    TextView TvAttendance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_attendance);

        new IntentIntegrator(this).initiateScan();
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
        integrator.setPrompt("Scan a barcode");
        integrator.setCameraId(1);
        integrator.setOrientationLocked(true);
    }

    @Override
    protected void onActivityResult ( int requestCode, int resultCode, Intent data){
        TvAttendance = (TextView) findViewById(R.id.tv_atten);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                TvAttendance.setText("your Absence is Marked..");
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                TvAttendance.setText("your Presence is Marked..");
                Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
