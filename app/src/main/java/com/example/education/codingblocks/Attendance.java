package com.example.education.codingblocks;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Attendance extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
    }

    public void MarkAttendance(View view){
        Intent i = new Intent(Attendance.this,MarkAttendance.class);
        startActivity(i);
        finish();
    }
    public void CheckAttendance(View view){
        Intent i = new Intent(Attendance.this,CheckAttendance.class);
        startActivity(i);
        finish();
    }
}
