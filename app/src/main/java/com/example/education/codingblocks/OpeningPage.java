package com.example.education.codingblocks;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class OpeningPage extends AppCompatActivity {

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opening_page);
        final Handler handler =new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent homeToLogin=new Intent(OpeningPage.this, SignIn.class);
                startActivity(homeToLogin);
                finish();
            }
        }, 1000);
    }
}